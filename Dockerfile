# Используем официальный образ Node.js
FROM node:22.2.0

# Устанавливаем рабочую директорию
WORKDIR /usr/src/app

# Копируем package.json и package-lock.json
COPY package*.json ./

# Устанавливаем зависимости внутри контейнера
RUN npm install

# Копируем все файлы проекта
COPY . .

# Компилируем TypeScript код
RUN npm run build

# Указываем команду для запуска приложения
CMD ["node", "dist/src/main.js"]

# Указываем порт, который будет использован
EXPOSE 3000
