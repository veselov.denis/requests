import { Injectable } from "@nestjs/common";
import * as fs from "fs";
import * as path from "path";

@Injectable()
export class EmailService {
  private readonly emailDir: string = path.join(__dirname, "../../emails_tmp");

  constructor() {
    if (!fs.existsSync(this.emailDir)) {
      fs.mkdirSync(this.emailDir, { recursive: true });
    }
  }

  async sendEmail({
    to,
    subject,
    body,
  }: {
    to: string;
    subject: string;
    body: string;
  }): Promise<void> {
    const emailContent: string = `To: ${to}\nSubject: ${subject}\n\n${body}`;
    const filePath: string = path.join(
      this.emailDir,
      `email-${Date.now()}.txt`,
    );
    await fs.promises.writeFile(filePath, emailContent);
  }
}
