export const PAGE_DEFAULT: number = 1;
export const LIMIT_DEFAULT: number = 10;
export const SORT_FIELD_DEFAULT: string = "id";
export const SORT_ORDER_DEFAULT: string = "DESC";
export const SORT_DEFAULT: string = `${SORT_FIELD_DEFAULT}:${SORT_ORDER_DEFAULT}`;
export const VALID_SORT_ORDERS: any = ["ASC", "DESC"];
export const VALID_SORT_FIELDS: any = [
  "id",
  "name",
  "email",
  "created_at",
  "updated_at",
];
