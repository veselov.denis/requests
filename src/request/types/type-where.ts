import { FindOperator } from "typeorm";
import { ERequestStatus } from "../enums/request-status.enum";

export type TypeWhere = {
  created_at?: FindOperator<Date>;
  status?: ERequestStatus;
  email?: FindOperator<string>;
};
