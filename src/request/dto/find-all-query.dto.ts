import {
  IsOptional,
  IsString,
  IsDateString,
  IsInt,
  Min,
  IsEnum,
} from "class-validator";
import { Type } from "class-transformer";
import { ERequestStatus } from "../enums/request-status.enum";
import { ApiPropertyOptional } from "@nestjs/swagger";

export class FindAllQueryDto {
  @IsOptional()
  @IsEnum(ERequestStatus, { message: "Invalid status value" })
  @ApiPropertyOptional({
    description: "Status of the request",
    enum: ERequestStatus,
  })
  status?: ERequestStatus;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ description: "Email of the requestor" })
  email?: string;

  @IsOptional()
  @IsDateString()
  @ApiPropertyOptional({
    description: "Start date for filtering requests",
    example: "2024-06-05T19:00:00.000Z",
  })
  fromDate?: string;

  @IsOptional()
  @IsDateString()
  @ApiPropertyOptional({
    description: "End date for filtering requests",
    example: "2024-06-07T07:00:00.000Z",
  })
  toDate?: string;

  @IsOptional()
  @IsInt()
  @Min(1)
  @ApiPropertyOptional({
    description: "Page number for pagination",
    minimum: 1,
  })
  @Type(() => Number)
  page?: number;

  @IsOptional()
  @IsInt()
  @Min(1)
  @ApiPropertyOptional({
    description: "Number of items per page for pagination",
    minimum: 1,
  })
  @Type(() => Number)
  limit?: number;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ description: "Sorting criteria" })
  sort: string;
}
