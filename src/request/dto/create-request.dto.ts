import { IsEmail, IsNotEmpty, Length } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateRequestDto {
  @IsNotEmpty()
  @Length(1, 100)
  @ApiProperty({
    description: "The name of the requestor",
    minLength: 1,
    maxLength: 100,
  })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    description: "The email of the requestor",
    required: true,
  })
  email: string;

  @IsNotEmpty()
  @Length(1, 1000)
  @ApiProperty({
    description: "The message of the request",
    required: true,
    minLength: 1,
    maxLength: 1000,
  })
  message: string;
}
