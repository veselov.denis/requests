import { IsNotEmpty, IsString, Length } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateRequestDto {
  @IsNotEmpty()
  @IsString()
  @Length(1, 1000)
  @ApiProperty({
    description: "Comment for the request",
    required: true,
    minLength: 1,
    maxLength: 1000,
  })
  comment: string;
}
