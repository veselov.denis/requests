import {
  SORT_FIELD_DEFAULT,
  SORT_ORDER_DEFAULT,
  VALID_SORT_ORDERS,
  VALID_SORT_FIELDS,
} from "../../config";

export function buildOrderClauseUtil(sort: string): { [key: string]: string } {
  let [sortField, sortOrder] = sort.split(":");

  sortField = sortField ? sortField.toLowerCase() : SORT_FIELD_DEFAULT;
  sortOrder = sortOrder ? sortOrder.toUpperCase() : SORT_ORDER_DEFAULT;

  if (
    !VALID_SORT_FIELDS.includes(sortField) ||
    !VALID_SORT_ORDERS.includes(sortOrder)
  ) {
    sortField = SORT_FIELD_DEFAULT;
    sortOrder = SORT_ORDER_DEFAULT;
  }

  return {
    [sortField]: sortOrder,
  };
}
