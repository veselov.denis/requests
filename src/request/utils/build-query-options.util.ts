import { FindManyOptions } from "typeorm";
import { Request } from "../entities/request.entity";
import { TypeWhere } from "../types/type-where";

export function buildQueryOptionsUtil(
  where: TypeWhere,
  order: { [key: string]: string },
): FindManyOptions<Request> {
  return {
    where,
    order,
  };
}
