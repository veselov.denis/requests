import { Between, Like } from "typeorm";
import { FindAllQueryDto } from "../dto/find-all-query.dto";
import { TypeWhere } from "../types/type-where";

export function buildWhereClauseUtil({
  status,
  email,
  fromDate,
  toDate,
}: FindAllQueryDto): TypeWhere {
  const where: TypeWhere = {};

  if (status) {
    where.status = status;
  }

  if (email) {
    where.email = Like(`%${email.toLowerCase().trim()}%`);
  }

  if (fromDate || toDate) {
    where.created_at = Between(
      fromDate ? new Date(fromDate) : new Date("1970-01-01"),
      toDate ? new Date(toDate) : new Date(),
    );
  }

  return where;
}
