import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { FindManyOptions, Repository } from "typeorm";
import { Request } from "./entities/request.entity";
import { EmailService } from "../services/email.service";
import { CreateRequestDto } from "./dto/create-request.dto";
import { UpdateRequestDto } from "./dto/update-request.dto";
import { FindAllQueryDto } from "./dto/find-all-query.dto";
import { LIMIT_DEFAULT, PAGE_DEFAULT, SORT_DEFAULT } from "../config";
import { paginate, Pagination } from "nestjs-typeorm-paginate";
import { buildQueryOptionsUtil } from "./utils/build-query-options.util";
import { buildOrderClauseUtil } from "./utils/build-order-clause.util";
import { buildWhereClauseUtil } from "./utils/build-where-clause.util";
import { TypeWhere } from "./types/type-where";

@Injectable()
export class RequestService {
  constructor(
    @InjectRepository(Request)
    private requestRepository: Repository<Request>,
    private emailService: EmailService,
  ) {}

  create(createRequestDto: CreateRequestDto): Promise<Request> {
    try {
      const request: Request = this.requestRepository.create(createRequestDto);
      return this.requestRepository.save(request);
    } catch (error) {
      throw new InternalServerErrorException("Failed to create requests");
    }
  }

  async findAll({
    status,
    email,
    fromDate,
    toDate,
    sort = SORT_DEFAULT,
    limit = LIMIT_DEFAULT,
    page = PAGE_DEFAULT,
  }: FindAllQueryDto): Promise<Pagination<Request>> {
    try {
      const where: Partial<TypeWhere> = buildWhereClauseUtil({
        status,
        email,
        fromDate,
        toDate,
        sort,
        page,
        limit,
      } as FindAllQueryDto);
      const order: { [p: string]: string } = buildOrderClauseUtil(sort);
      const options: FindManyOptions<Request> = buildQueryOptionsUtil(
        where,
        order,
      );

      return paginate<Request>(
        this.requestRepository,
        { page, limit },
        options,
      );
    } catch (error) {
      throw new InternalServerErrorException("Failed to fetch requests");
    }
  }

  async update(id: number, updateRequestDto: UpdateRequestDto): Promise<void> {
    let request: Request | undefined;
    try {
      request = await this.requestRepository.findOne({
        where: { id },
      });
    } catch (error) {
      throw new InternalServerErrorException("Failed to find request");
    }

    if (!request) {
      throw new NotFoundException(`Request with id ${id} not found`);
    }

    try {
      request.comment = updateRequestDto.comment;
      await this.requestRepository.save(request);
    } catch (error) {
      throw new InternalServerErrorException("Failed to update request");
    }

    try {
      await this.emailService.sendEmail({
        to: request.email,
        subject: "Ваша заявка завершена",
        body: `Ваша заявка была обработана. \nКомментарий: ${request.comment}`,
      });
    } catch (error) {
      throw new InternalServerErrorException(
        "Request updated but failed to send email",
      );
    }
  }
}
