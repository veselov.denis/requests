import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { ERequestStatus } from "../enums/request-status.enum";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
@Index(["email"])
@Index(["status"])
@Index(["created_at"])
@Index(["updated_at"])
export class Request {
  @PrimaryGeneratedColumn()
  @ApiProperty({ description: "The unique identifier of the request" })
  id: number;

  @Column("varchar", { length: 100 })
  @ApiProperty({ description: "The name of the requestor" })
  name: string;

  @Column("varchar")
  @ApiProperty({ description: "The email of the requestor" })
  email: string;

  @Column({
    type: "enum",
    enum: ERequestStatus,
    default: ERequestStatus.Active,
  })
  @ApiProperty({
    description: "The status of the request",
    enum: ERequestStatus,
  })
  status: ERequestStatus;

  @Column("text")
  @ApiProperty({ description: "The message of the request" })
  message: string;

  @Column("text", { nullable: true })
  @ApiProperty({ description: "The comment on the request" })
  comment: string;

  @CreateDateColumn({ type: "timestamptz" })
  @ApiProperty({ description: "The date the request was created" })
  created_at: Date;

  @UpdateDateColumn({
    type: "timestamptz",
    nullable: true,
    default: (): string => "NULL",
  })
  @ApiProperty({ description: "The date the request was last updated" })
  updated_at: Date;

  @BeforeUpdate()
  updateStatusOnComment(): void {
    if (this.comment && this.status !== ERequestStatus.Resolved) {
      this.status = ERequestStatus.Resolved;
    }
  }

  @BeforeInsert()
  emailToLowerCase(): void {
    if (this.email) {
      this.email = this.email.toLowerCase();
    }
  }
}
