import { Test, TestingModule } from "@nestjs/testing";
import { INestApplication, ValidationPipe } from "@nestjs/common";
import request from "supertest";
import { AppModule } from "../app.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Request } from "./entities/request.entity";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { CreateRequestDto } from "./dto/create-request.dto";
import { execSync } from "child_process";
import { join } from "path";
import { ERequestStatus } from "./enums/request-status.enum";

describe("RequestController (e2e)", () => {
  let app: INestApplication;
  let repository: Repository<Request>;
  const createdRequests: CreateRequestDto[] = [
    {
      name: "John Doe",
      email: "unique@example.com",
      message: "This is a test message",
    },
    {
      name: "Jane Doe",
      email: "another@example.com",
      message: "Another test message",
    },
  ];

  beforeAll(async () => {
    execSync("npm run migration:run");

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ".env.test",
        }),
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule],
          useFactory: (configService: ConfigService) => ({
            type: "postgres",
            host: configService.get<string>("DB_HOST"),
            port: configService.get<number>("DB_PORT"),
            username: configService.get<string>("DB_USERNAME"),
            password: configService.get<string>("DB_PASSWORD"),
            database: configService.get<string>("DB_DATABASE"),
            entities: [Request],
            synchronize: false,
            migrationsRun: true,
            migrations: [join(__dirname, "/../migrations/*{.ts,.js}")],
          }),
          inject: [ConfigService],
        }),
        AppModule,
      ],
    }).compile();

    repository = moduleFixture.get<Repository<Request>>(
      getRepositoryToken(Request),
    );
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    await repository.query('DELETE FROM "request"');

    for (const createRequestDto of createdRequests) {
      await request(app.getHttpServer())
        .post("/requests")
        .send(createRequestDto)
        .expect(201);
    }
  });

  afterAll(async () => {
    await repository.query('DELETE FROM "request"');
    await app.close();
  });

  it("should find requests by exact email match", async () => {
    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        email: "unique@example.com",
        page: 1,
        limit: 10,
        sort: "id:asc",
      })
      .expect(200);

    expect(response.body.items).toHaveLength(1);
    expect(response.body.items[0].email).toBe("unique@example.com");
  });

  it("should find requests by partial email match", async () => {
    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        email: "unique@",
        page: 1,
        limit: 10,
        sort: "id:asc",
      })
      .expect(200);

    expect(response.body.items).toHaveLength(1);
    expect(response.body.items[0].email).toBe("unique@example.com");
  });

  it("should find requests within a time range", async () => {
    const fromDate = new Date(
      new Date().getTime() - 1000 * 60 * 60,
    ).toISOString();
    const toDate = new Date(
      new Date().getTime() + 1000 * 60 * 60,
    ).toISOString();

    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        fromDate,
        toDate,
        page: 1,
        limit: 10,
        sort: "id:asc",
      })
      .expect(200);

    expect(response.body.items).toHaveLength(2);
    expect(response.body.items[0].email).toBe("unique@example.com");
    expect(response.body.items[1].email).toBe("another@example.com");
  });

  it("should not find requests outside a time range", async () => {
    const pastFromDate = new Date(
      new Date().getTime() - 1000 * 60 * 60 * 24 * 7,
    ).toISOString();
    const pastToDate = new Date(
      new Date().getTime() - 1000 * 60 * 60 * 24 * 6,
    ).toISOString();

    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        fromDate: pastFromDate,
        toDate: pastToDate,
        page: 1,
        limit: 10,
        sort: "id:asc",
      })
      .expect(200);

    expect(response.body.items).toHaveLength(0);
  });

  it("should find requests sorted by name in ascending order", async () => {
    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        sort: "name:asc",
        page: 1,
        limit: 10,
      })
      .expect(200);

    expect(response.body.items).toHaveLength(2);
    expect(response.body.items[0].name).toBe("Jane Doe");
    expect(response.body.items[1].name).toBe("John Doe");
  });

  it("should find requests sorted by name in descending order", async () => {
    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        sort: "name:desc",
        page: 1,
        limit: 10,
      })
      .expect(200);

    expect(response.body.items).toHaveLength(2);
    expect(response.body.items[0].name).toBe("John Doe");
    expect(response.body.items[1].name).toBe("Jane Doe");
  });

  it("should find all requests with status Active and updated_at null", async () => {
    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        status: "Active",
        page: 1,
        limit: 10,
        sort: "id:asc",
      })
      .expect(200);

    expect(response.body.items).toHaveLength(2);
    response.body.items.forEach((item) => {
      expect(item.status).toBe(ERequestStatus.Active);
      expect(item.updated_at).toBeNull();
    });
  });

  it("should paginate requests and return the first page", async () => {
    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        page: 1,
        limit: 2,
        sort: "id:asc",
      })
      .expect(200);

    expect(response.body.items).toHaveLength(2);
    expect(response.body.meta.totalItems).toBe(2);
    expect(response.body.meta.itemCount).toBe(2);
    expect(response.body.meta.itemsPerPage).toBe(2);
    expect(response.body.meta.totalPages).toBe(1);
    expect(response.body.meta.currentPage).toBe(1);
    expect(response.body.items[0].name).toBe("John Doe");
    expect(response.body.items[1].name).toBe("Jane Doe");
  });

  it("should paginate requests and return the second page", async () => {
    const additionalRequest: CreateRequestDto = {
      name: "Alice Doe",
      email: "alice@example.com",
      message: "Message from Alice",
    };

    await request(app.getHttpServer())
      .post("/requests")
      .send(additionalRequest)
      .expect(201);

    const response = await request(app.getHttpServer())
      .get("/requests")
      .query({
        page: 2,
        limit: 2,
        sort: "id:asc",
      })
      .expect(200);

    expect(response.body.items).toHaveLength(1);
    expect(response.body.meta.totalItems).toBe(3);
    expect(response.body.meta.itemCount).toBe(1);
    expect(response.body.meta.itemsPerPage).toBe(2);
    expect(response.body.meta.totalPages).toBe(2);
    expect(response.body.meta.currentPage).toBe(2);
    expect(response.body.items[0].name).toBe("Alice Doe");
  });

  it("should update the request with a comment, change status to Resolved, and update updated_at", async () => {
    const createRequestDto: CreateRequestDto = {
      name: "Test User",
      email: "testuser@example.com",
      message: "Test message for update",
    };

    const createResponse = await request(app.getHttpServer())
      .post("/requests")
      .send(createRequestDto)
      .expect(201);

    const requestId = createResponse.body.id;

    await request(app.getHttpServer())
      .put(`/requests/${requestId}`)
      .send({ comment: "Updated comment" })
      .expect(200);

    const getResponse = await request(app.getHttpServer())
      .get("/requests")
      .query({
        email: "testuser@example.com",
        page: 1,
        limit: 10,
        sort: "id:asc",
      })
      .expect(200);

    const updatedRequest = getResponse.body.items.find(
      (item) => item.id === requestId,
    );

    expect(updatedRequest.comment).toBe("Updated comment");
    expect(updatedRequest.status).toBe(ERequestStatus.Resolved);

    const updatedAt = new Date(updatedRequest.updated_at);
    const now = new Date();
    const timeDifference = Math.abs(now.getTime() - updatedAt.getTime());

    expect(timeDifference).toBeLessThan(30 * 1000);
  });
});
