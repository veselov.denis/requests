import { Module } from "@nestjs/common";
import { RequestService } from "./request.service";
import { RequestController } from "./request.controller";
import { Request } from "./entities/request.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { EmailService } from "../services/email.service";

@Module({
  imports: [TypeOrmModule.forFeature([Request])],
  controllers: [RequestController],
  providers: [RequestService, EmailService],
})
export class RequestModule {}
