import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Query,
  UseGuards,
  NotFoundException,
  InternalServerErrorException,
  BadRequestException,
} from "@nestjs/common";
import { RequestService } from "./request.service";
import { CreateRequestDto } from "./dto/create-request.dto";
import { UpdateRequestDto } from "./dto/update-request.dto";
import { Request } from "./entities/request.entity";
import { FindAllQueryDto } from "./dto/find-all-query.dto";
import { Pagination } from "nestjs-typeorm-paginate";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from "@nestjs/swagger";

@ApiTags("requests")
@Controller("requests")
export class RequestController {
  constructor(private readonly requestService: RequestService) {}

  @Post()
  @ApiOperation({ summary: "Create a new request" })
  @ApiBody({
    type: CreateRequestDto,
    examples: {
      a: {
        summary: "Example successful request",
        value: {
          name: "John Doe",
          email: "johndoe@example.com",
          message: "This is a sample request message.",
        },
      },
      b: {
        summary: "Example failed request",
        value: {
          name: "John Smith",
          email: "",
          message: "This is a sample request message.",
        },
      },
    },
  })
  create(@Body() createRequestDto: CreateRequestDto) {
    try {
      return this.requestService.create(createRequestDto);
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw new BadRequestException(error.message);
      } else {
        throw new InternalServerErrorException("Failed to create request");
      }
    }
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: "Get all requests" })
  @ApiBearerAuth()
  @ApiQuery({
    description: "Find requests by status (e.g., Active, Resolved)",
    name: "status",
    required: false,
    type: String,
    example: "Active",
  })
  @ApiQuery({
    description: "Find requests by email or part of the email",
    name: "email",
    required: false,
    type: String,
    example: "johndoe@example.com",
  })
  @ApiQuery({
    description: "Start date for filtering requests",
    name: "fromDate",
    required: false,
    type: String,
    example: "2023-12-31T00:00:00.000Z",
  })
  @ApiQuery({
    description: "End date for filtering requests",
    name: "toDate",
    required: false,
    type: String,
    example: "2024-06-30T23:59:59.999Z",
  })
  @ApiQuery({
    description: "Page number for pagination",
    name: "page",
    required: false,
    type: Number,
    example: 1,
  })
  @ApiQuery({
    description: "Number of items per page for pagination",
    name: "limit",
    required: false,
    type: Number,
    example: 10,
  })
  @ApiQuery({
    description: `Sorting criteria in the format "field:direction" ('id', 'name', 'email', 'created_at', 'updated_at' fields is available)`,
    name: "sort",
    required: false,
    type: String,
    example: "name:asc",
  })
  findAll(@Query() queryParams: FindAllQueryDto): Promise<Pagination<Request>> {
    try {
      return this.requestService.findAll(queryParams);
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw new BadRequestException(error.message);
      } else {
        throw new InternalServerErrorException("Failed to fetch requests");
      }
    }
  }

  @Put(":id")
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: "Resolve a request" })
  @ApiBearerAuth()
  @ApiBody({
    type: UpdateRequestDto,
    examples: {
      a: {
        summary: "Example successful request",
        value: {
          comment: "This is a sample comment.",
        },
      },
      b: {
        summary: "Example failed request",
        value: {
          comment: "",
        },
      },
    },
  })
  async update(
    @Param("id") id: string,
    @Body() updateRequestDto: UpdateRequestDto,
  ): Promise<void> {
    const numericId: number = Number(id);

    if (isNaN(numericId)) {
      throw new BadRequestException("Invalid ID format. ID must be a number.");
    }

    try {
      await this.requestService.update(numericId, updateRequestDto);
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException(error.message);
      } else {
        throw new InternalServerErrorException("Failed to update request");
      }
    }
  }
}
