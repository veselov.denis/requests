import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { DatabaseModule } from "./database/database.module";
import { RequestModule } from "./request/request.module";
import { AuthModule } from "./auth/auth.module";
import { AuthController } from "./auth/auth.controller";
import { ManagerModule } from "./manager/manager.module";
import { AllExceptionsFilter } from "./exceptions/all-exceptions.filter";
import { APP_FILTER } from "@nestjs/core";
import { ErrorHandlingModule } from "./exceptions/error-handling.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
    }),
    DatabaseModule,
    RequestModule,
    AuthModule,
    ManagerModule,
    AppModule,
    ErrorHandlingModule,
  ],
  controllers: [AuthController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
})
export class AppModule {}
