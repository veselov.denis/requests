import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Request } from "../request/entities/request.entity";
import { Manager } from "../manager/entities/manager.entity";

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: "postgres",
        host: configService.get<string>("DB_HOST"),
        port: configService.get<number>("DB_PORT"),
        username: configService.get<string>("DB_USERNAME"),
        password: configService.get<string>("DB_PASSWORD"),
        database: configService.get<string>("DB_DATABASE"),
        entities: [Request, Manager],
        autoLoadEntities: true,
        synchronize: false,
        migrationsRun: true,
        migrations: ["dist/src/database/migrations/*.js"],
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
