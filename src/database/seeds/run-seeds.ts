import { AppDataSource } from "../../../ormconfig";
import seedManagers from "../../manager/seeds/manager.seed";

async function runSeeds() {
  await AppDataSource.initialize();
  console.log("Data Source has been initialized!");

  await seedManagers(AppDataSource);
  console.log("Managers have been seeded!");

  await AppDataSource.destroy();
}

runSeeds().catch((err) => {
  console.error("Error during seeding:", err);
});
