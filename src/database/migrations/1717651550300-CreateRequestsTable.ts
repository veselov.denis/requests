import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateRequestsTable1717651550300 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      CREATE TYPE "request_status_enum" AS ENUM('Active', 'Resolved');
      CREATE TABLE "request" (
        "id" SERIAL PRIMARY KEY,
        "name" character varying(100) NOT NULL,
        "email" character varying NOT NULL,
        "status" "request_status_enum" NOT NULL DEFAULT 'Active',
        "message" text NOT NULL,
        "comment" text,
        "created_at" TIMESTAMPTZ NOT NULL DEFAULT now(),
        "updated_at" TIMESTAMPTZ
      )
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "request"`);
    await queryRunner.query(`DROP TYPE "request_status_enum"`);
  }
}
