import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateManagersTable1717661971941 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      CREATE TYPE "manager_role_enum" AS ENUM('manager', 'support');
      CREATE TABLE "manager" (
        "id" SERIAL NOT NULL,
        "username" character varying(100) NOT NULL,
        "password" character varying NOT NULL,
        "role" "manager_role_enum" NOT NULL DEFAULT 'manager',
        CONSTRAINT "PK_manager_id" PRIMARY KEY ("id"),
        CONSTRAINT "unique_manager_username" UNIQUE ("username")
      )
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      DROP TABLE "manager"
    `);
    await queryRunner.query(`
      DROP TYPE "manager_role_enum"
    `);
  }
}
