import {
  Controller,
  InternalServerErrorException,
  Post,
  Request,
  UnauthorizedException,
  UseGuards,
} from "@nestjs/common";
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./local-auth.guard";
import { ApiBearerAuth, ApiBody, ApiOperation, ApiTags } from "@nestjs/swagger";
import { LoginManagerDto } from "./dto/login-manager.dto";

@ApiTags("auth")
@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post("login")
  @ApiOperation({ summary: "User login" })
  @ApiBearerAuth()
  @ApiBody({
    type: LoginManagerDto,
    examples: {
      a: {
        summary: "Example successful request",
        value: {
          username: "manager",
          password: "password",
        },
      },
      b: {
        summary: "Example failed request",
        value: {
          username: "user",
          password: "password",
        },
      },
    },
  })
  async login(@Request() req) {
    try {
      return this.authService.login(req.user);
    } catch (error) {
      if (error instanceof UnauthorizedException) {
        throw new UnauthorizedException("Invalid credentials");
      } else {
        throw new InternalServerErrorException(
          "An error occurred during login",
        );
      }
    }
  }
}
