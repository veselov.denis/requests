import { IsEmail, IsNotEmpty, Length } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class LoginManagerDto {
  @IsNotEmpty()
  @Length(1, 100)
  @ApiProperty({
    description: "The username of the manager",
    required: true,
    minLength: 1,
    maxLength: 100,
  })
  username: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ description: "The password of the manager", required: true })
  password: string;
}
