import { Entity, PrimaryGeneratedColumn, Column, Unique } from "typeorm";
import { EManagerRole } from "../enums/manager-role.enum";

@Entity()
export class Manager {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  @Unique("manager", ["username"])
  username: string;

  @Column()
  password: string;

  @Column({ type: "enum", enum: EManagerRole, default: EManagerRole.Manager })
  role: string;
}
