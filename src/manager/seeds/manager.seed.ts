import { DataSource } from "typeorm";
import { Manager } from "../entities/manager.entity";
import * as bcrypt from "bcrypt";
import { EManagerRole } from "../enums/manager-role.enum";

async function seedManagers(dataSource: DataSource) {
  const salt: string = await bcrypt.genSalt();
  const password: string = await bcrypt.hash("password", salt);

  await dataSource
    .createQueryBuilder()
    .insert()
    .into(Manager)
    .values([
      { username: "manager", password, role: EManagerRole.Manager },
      { username: "support", password, role: EManagerRole.Support },
    ])
    .execute();
}

export default seedManagers;
