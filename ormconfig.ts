import { ConfigModule, ConfigService } from "@nestjs/config";
import { DataSource, DataSourceOptions } from "typeorm";
import { Request } from "./src/request/entities/request.entity";
import { Manager } from "./src/manager/entities/manager.entity";

ConfigModule.forRoot({
  isGlobal: true,
  envFilePath: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
});

const configService = new ConfigService();

const config: DataSourceOptions = {
  type: "postgres",
  host: configService.get<string>("DB_HOST"),
  port: configService.get<number>("DB_PORT"),
  username: configService.get<string>("DB_USERNAME"),
  password: configService.get<string>("DB_PASSWORD"),
  database: configService.get<string>("DB_DATABASE"),
  entities: [Request, Manager],
  synchronize: false,
  migrations: ["dist/src/database/migrations/*.js"],
};

export const AppDataSource: DataSource = new DataSource(config);
export default config;
