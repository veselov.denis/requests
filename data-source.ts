import { DataSource } from "typeorm";
import config from "./ormconfig";

export const AppDataSource: DataSource = new DataSource(config);

AppDataSource.initialize()
  .then((): void => {
    console.log("Data Source has been initialized!");
  })
  .catch((err): void => {
    console.error("Error during Data Source initialization", err);
  });
