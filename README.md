# Тестовое задание
## Общие положения:
- Данное тестовое задание не направлено на полноценную реализацию задачи, сколько на возможность продемонстрировать уровень понимая в таких вопросах, как: node.js, REST API, работа с СУБД, основы безопасности. Помните, у задания не существует единственно правильного решения, но каждому вашему решению должно находиться объяснение;
- Постарайтесь показать свои лучшие практики написания кода;
- В процессе выполнения можно использовать любые библиотеки и фреймворки,
если они позволяют реализовать задачу качественно и быстрее, но в случае использования библиотек будьте готовы объяснить, почему используется конкретная библиотека, если появятся таковые вопросы;
- Тем не менее, предпочтительна реализация на Nest.js. В качестве СУБД использовать PostgreSQL актуальной стабильной версии;
- Предпочтительно выложить код в GIt репозиторий. По возможности, ведите историю коммитов, чтобы мы могли отследить Ваш процесс разработки;
- По возможности и при наличии времени, очищайте неиспользуемый шаблонный код.

## Общее описание:
Необходимо реализовать систему принятия и обработки заявок пользователей с сайта. Любой пользователь может отправить данные по публичному API, реализованному нами, оставив заявку с каким-то текстом,. Затем заявка рассматривается ответственным лицом и ей устанавливается статус Завершено. Чтобы установить этот статус, ответственное лицо должно оставить комментарий. Пользователь должен получить свой ответ по email.
При этом, ответственное лицо должно иметь возможность получить список заявок, отфильтровать их по статусу и по дате, а также иметь возможность ответить задающему вопрос через email.

### Сущности:

| Поле        | Тип данных                      | Описание                                                       | Обязательность                               |
|-------------|---------------------------------|---------------------------------------------------------------|----------------------------------------------|
| `id`        | Уникальный идентификатор        | Уникальный идентификатор                                       | Да                                           |
| `name`      | Строка                          | Имя пользователя                                               | Да                                           |
| `email`     | Строка                          | Email пользователя                                             | Да                                           |
| `status`    | Enum ("Active", "Resolved")     | Статус заявки                                                  | Да                                           |
| `message`   | Текст                           | Сообщение пользователя                                         | Да                                           |
| `comment`   | Текст                           | Ответ ответственного лица                                      | Да, если статус "Resolved"                   |
| `created_at`| timestamp или datetime          | Время создания заявки                                          | Да                                           |
| `updated_at`| timestamp или datetime          | Время ответа на заявку                                         | Нет                                          |

### Endpointы API:

Методы API должны быть документированы каким-нибудь средством документации на ваш выбор. Предпочтительно, с наличием песочницы.
- **GET** `/requests/` - получение заявок ответственным лицом, с фильтрацией по статусу
- **PUT** `/requests/{id}/` - ответ на конкретную задачу ответственным лицом
- **POST** `/requests/` - отправка заявки пользователями системы

## Дополнения:

- Вы можете дополнять задачу отдельными методами и расширять объем входящих параметров, если посчитаете нужным;
- Вы можете сделать авторизацию как и для публичного пользователя, так и для ответственного лица так, как вы посчитаете нужным;
- Вы можете сами рассмотреть особенности безопасности входящих запросов, чтобы избежать кроссдоменных запросов или же наоборот, разрешить их безопасно;
- Вам необязательно делать web интерфейс для отправки заявок и ответа на них;
- Вам разрешено делать дополнительные улучшения, дополнительные фильтрации и методы API, но будьте готовы их прокомментировать;
- Вам также разрешено переименовать поля сущностей и добавить новые, если вы считаете, что они приведут к большему пониманию того, что происходит в предметной области задачи или расширят ее (Например, endpoint удаления задачи, прикрепление за заявкой ответственного лица и д.р.);
- Дополнительно будет преимуществом, если вы представите себе, что заявки отправляются очень часто и храниться их может огромное количество;
- Для отправки email можете воспользоваться NullObject реализацией какого- нибудь стандартного интерфейса, либо же сохранять email в виде plain файлов в директории временных файлов вашего фреймворка или по вашему выбору;
- Unit тесты также приветствуются.

---

## Запуск приложения

### Шаги для запуска приложения:

1. Клонируйте проект:
```
git clone https://gitlab.com/veselov.denis/requests.git
```
2. Перейдите в директорию проекта:
```
cd requests
```
3. Добавьте файл с переменными окружения в корень проекта:

- `.env`:
```
NODE_ENV=production
DB_USERNAME=pguser
DB_PASSWORD=pgpass
DB_DATABASE=requestsdb
DB_HOST=localhost
DB_PORT=5432
JWT_SECRET=G3Q2SpviwxW5HxTpzUM0YMVnNZdmkenVUGg64MLjWJYcLtJ
```

4. Запустите docker-compose:
```
docker-compose up
```

5. Сервис слушает порт 3000

```
http://localhost:3000

```

## Описание приложения

Используемый стек:

- `Nest.js`
- `TypeORM`
- `PostgreSQL`

Приложение API запускается вместе с БД. При развертывании автоматически выполняются миграции и сиды для заполнения БД исходными данными об ответственных лицах.

Проект включает в себя сервисы:

- `api` - Приложение API (Nest.js). Предоставляет роуты для создания заявок, получения информации о заявках, ответа на заявку и авторизации ответственного лица. При ответе на заявку имитируется отправка email (сохранение текстового файла).
- `db` - База данных (PostgreSQL)

### Исходные данные в БД
После запуска проекта в базе данных создаются записи ответственных лиц. Доступы для аутентификации: 
- Логин: `manager`
- Пароль: `password`

или
- Логин: `support`
- Пароль: `password`

### OpenAPI

Ссылка на OpenAPI:
```
http://localhost:3000/docs
```

### API
Роуты API доступны по адресу:
```
http://localhost:3000
```

- Создание заявки:
```
POST /requests
Создает заявку

name (string) - Имя пользователя
email (string) - Email пользователя
message (string) - Текст заявки

Метод доступен без авторизации.
```

- Аутентификация ответственного лица:
```
POST /auth/login

Возвращает токен доступа для защищенных методов. 
Время жизни токена - 1 час.

username (string) - Логин
password (string) - Пароль
```
- Получение списка заявок:
```
GET /requests
Authorization: Bearer {access_token}

Возвращает список заявок и метаданные для пагинации:

Доступные параметры для фильтрации, сортировки и пагинации:

status=Active - Фильтрация по статусу заявки
email=@email.com - Фильтрация по частичному совпадению email заявки
fromDate=2024-07-06T02:05:00Z - Начало периода для выборки (по времени создания)
toDate=2024-06-10T20:24:00Z - Конец периода для выборки (по времени создания)
sort=id:desc - Сортировка по полям (id, email, дата создания, дата изменения)
limit=10 - Количество заявок на странице
page=2 - Номер страницы

Все параметры опциональны.
```

- Добавление ответа на заявку:
```
PUT /requests
Authorization: Bearer {access_token}

comment (string) - Текст ответа на заявку

При добавлении ответа заявка автоматически переводится в статус "Resolved". 
```

### База данных

БД содержит таблицы
- `request` - Таблица заявок
- `manager` - Таблица сотрудников

Доступы к БД:

```
Database: requestsdb
User: pguser
Password: pgpass
Host: localhost
Port: 5432
```

### Предложения для дальнейших улучшений проекта

- Продолжить разделение логики на слои (контроллеры, сервисы) для улучшения поддержки и масштабирования,
- Добавить юнит-тестирование, доработать интеграционное тестирование,
- Поработать над типизацией,
- Реализовать обработку цифровой подписи (сигнатуры) запросов в целях повышения безопасности,
- Добавить CORS-политики.
